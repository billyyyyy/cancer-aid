import React from 'react';
import PropTypes from 'prop-types';

const ReferType = (props) => {
  const handleClick = () =>{
    props.setSelected(props.value)
  }

  // const TypeIcon = components[props.image]

  return (
    <div className="refer-type w-full my-2 px-2 lg:w-1/2 lg:my-0">
      <div 
        className={`
          flex
          border-2
          p-5
          rounded-md
          cursor-pointer
          relative
          hover:border-primary-light
          ${props.selected ? 'border-primary-light' : 'border-gray-border'}
        `}
        onClick={handleClick}
      >
        { 
          props.selected && 
          <span className="absolute bg-primary-light top-0 right-0 w-5 p-1 rounded-bl-md">
            <img src="/checkmark.svg" />
          </span>
        }
        <div className="image w-24 mr-3">
          <div className="p-2 rounded-full bg-primary-lighter">
            <props.image className="text-primary" />
          </div>
        </div>
        <div className="info">
          <h5 className="font-display mb-2">{props.title}</h5>
          <p className="leading-tight">{props.details}</p>
        </div>
      </div>
    </div>
  )
}

ReferType.propTypes = {
  details: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  selected: PropTypes.bool.isRequired,
  setSelected: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
}

export default ReferType
import React, { useState } from 'react';
import ReferType from './ReferType'
import GoalIcon from 'components/Icons/Goal'
import CoachIcon from 'components/Icons/Coach'

const types = [
  {
    key: 'referral',
    title: 'Referral',
    details: 'The customer would like to start the program',
    image: GoalIcon
  },
  {
    key: 'infoCall',
    title: 'Info call',
    details: 'The customer requests more information',
    image: CoachIcon
  }
]

const ReferTypes = () => {
  // TODO: handle this in Redux
  const [selected, setSelected] = useState(null)

  return(
    <div className="refer-types flex justify-between -mx-2 mb-10 flex-col lg:flex-row">
      {
        types.map((type) => 
          <ReferType
            details={type.details}
            image={type.image}
            key={type.key}
            selected={type.key === selected}
            setSelected={setSelected}
            title={type.title}
            value={type.key}
          />
        )
      }
    </div>
  )
}

export default ReferTypes
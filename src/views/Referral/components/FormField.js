import React from 'react'
import PropTypes from 'prop-types'
import { Input, Select, Checkbox } from 'components'

const FormField = (props) => {
  let input
  switch (props.type) {
    case 'select':
      input = <Select options={props.options}/>
      break
    case 'checkbox':
      input = <Checkbox />
      break;
    default:
      input = <Input
        type={props.type}
        value={props.value}
      />
  }

  return (
    <div className="form-field px-2 mb-8 w-full md:w-1/2">
      <div className="container w-full">
        { props.label && <label className="block text-gray-dark mb-2">{props.label}</label> }
        {input}
      </div>
    </div>
  )
}

FormField.propTypes = {
  label: PropTypes.string,
  options: PropTypes.array,
  type: PropTypes.string,
  value: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ])
}

export default FormField
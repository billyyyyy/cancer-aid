import React from 'react';
import ReferTypes from './ReferTypes'
import ReferForm from './ReferForm'
import clientConfig from 'config'

const ReferCustomerForm = () => {
  return (
    <div className="refer-customer-form w-full lg:w-3/5 px-10 pt-5">
      <div className="logo h-16 mb-20">
        <img
          className="h-full"
          src={clientConfig.logo}
        />
      </div>
      <h1 className="font-display text-3xl text-gray-dark mb-8">Refer a customer</h1>
      <ReferTypes />
      <ReferForm />
    </div>
  )
}

export default ReferCustomerForm
import React from 'react';
import PropTypes from 'prop-types'

const Tips = (props) => {
  return (
    <a
      className="my-2 bg-white border-2 border-gray-light flex p-5 rounded-md"
      href={props.link}
      target="_blank"
    >
      <span className="w-full">{props.label}</span>
      <div className="pl-2 w-6 flex items-center">
        <img src="/arrow-right.png" />
      </div>
    </a>
  )
}

Tips.propTypes = {
  label: PropTypes.string,
  link: PropTypes.string
}

export default Tips
import React from 'react';
import Tip from './Tip'
import clientConfig from 'config'

const tips = [
  { label: 'Not comfortable wit technology/apps', link: 'https://www.canceraid.com/' },
  { label: 'Already have enough support', link: 'https://www.canceraid.com/' },
  { label: 'Overwhelmed with informtion or options', link: 'https://www.canceraid.com/' }
]

const ConversationTips = () => {
  return (
    <div className="conversation-tips w-full bg-gray mt-20 lg:w-2/5 lg:mt-0">
      <div className="py-20 w-5/6 mx-auto">
        <img
          className="block w-full mb-12"
          src="/illustration.png"
        />
        <h1 className="font-display text-2xl text-gray-dark mb-5">Conversation tips</h1>
        <p className="leading-tight text-gray-dark mb-5">You may want to use this section to help guide your conversation with a customer</p>
        <div className="border-b-2 border-gray-light flex h-10 mb-5">
          <div className="h-full w-8 p-1 flex items-center"><img src="/search.svg" /></div>
          <input
            className="bg-transparent px-3 outline-none w-full"
            placeholder="Search"
            type="text"
          />
        </div>
        {
          clientConfig.showTips && tips.map((tip) => {
            return(
              <Tip
                key={tip.label}
                label={tip.label}
                link={tip.link}
              />
            )
          })
        }
      </div>
    </div>
  )
}

export default ConversationTips
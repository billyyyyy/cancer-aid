import React, {useState} from 'react'
import { Checkbox, Button } from 'components'
import FormField from './FormField'

const specialistOptions = [
  { label: 'Adam Fitzpatrick', value: 'Adam Fitzpatrick' },
  { label: 'Option2', value: 'Option2' },
  { label: 'Option3', value: 'Option3' },
  { label: 'Option4', value: 'Option4' },
]

const ReferForm = () => {
  // TODO: handle form values change with setFormState
  const [formState] = useState({
    name: '',
    dob: '',
    phone: '',
    email: '',
    claimNumber: '',
    specialist: '',
    consent: false
  })

  // const handleSubmit = () =>{
  //   // TODO: handle from submit
  //   // check validation
  // }

  return (
    <div className="refer-form">
      <div className="form flex justify-between flex-wrap -mx-2">
        <FormField
          label="Full name"
          value={formState.name}
        />
        <FormField
          label="Date of birth"
          value={formState.dob}
        />
        <FormField
          label="Phone number"
          type="phone"
          value={formState.phone}
        />
        <FormField
          label="Email address"
          value={formState.email}
        />
        <FormField
          label="Claim number"
          value={formState.claimNumber}
        />
        <FormField
          label="Specialist"
          options={specialistOptions}
          type="select"
          value={formState.specialist}
        />
        <div className="flex px-2 mb-10">
          <div className="mr-3">
            <Checkbox value={formState.consent} />
          </div>
          <p className="leading-none text-gray-dark text-sm">This customer consents for their contact details to be forwarded by xxx and to be contacted by CancerAid for the purposes of the CancerAid Coach Program.</p>
        </div>
        <div className="text-center w-full">
          <Button>Submit</Button>
        </div>
      </div>
    </div>
  )
}

export default ReferForm
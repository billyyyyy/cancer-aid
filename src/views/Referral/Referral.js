import React from 'react'
import ReferCustomerForm from './components/ReferCustomerForm'
import ConversationTips from './components/ConversationTips'

const Referral = () => {
  return (
    <div className="container w-full min-h-screen lg:w-5/6 xl:w-1280 mx-auto flex flex-col lg:flex-row">
      <ReferCustomerForm />
      <ConversationTips />
    </div>
  )
}

export default Referral
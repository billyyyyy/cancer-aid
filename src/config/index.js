const merge = require('merge')
const base = require('clients/base/config')
const brand = require(`clients/${process.env.REACT_APP_BRAND ? process.env.REACT_APP_BRAND : 'default'}/config`)

module.exports = merge(base, brand)
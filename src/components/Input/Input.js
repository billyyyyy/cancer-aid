import React from 'react'
import PropTypes from 'prop-types'

const Input = (props) => {
  const styleClasses = 'w-full border-2 border-gray-light rounded-md h-12 bg-gray-lighter text-gray-dark font-body outline-none focus:shadow-sm'
  let input
  switch (props.type) {
    case 'phone':
      input = <div className={`${styleClasses} p-0 flex`}>
        <span className="flex justify-center items-center w-12 border-r-2 border-gray-light">+61</span>
        <input
          className="h-full w-full outline-none bg-gray-lighter px-2"
          type="text"
        />
      </div>
      break
    default:
      input = <input
        className={`${styleClasses} px-2`}
        type="text"
      />
  }
  
  return input
}

Input.propTypes = {
  type: PropTypes.string,
  value: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ])
}

export default Input
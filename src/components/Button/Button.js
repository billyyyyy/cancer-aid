import React from 'react'
import PropTypes from 'prop-types'

const Button = (props) => {
  return (
    <button className="bg-primary py-3 px-5 min-w-32 text-white text-sm rounded shadow-button outline-none">
      {props.children || 'Button'}
    </button>
  )
}

Button.propTypes = {
  children: PropTypes.any
}

export default Button
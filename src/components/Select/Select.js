import React from 'react'
import PropTypes from 'prop-types'

const Select = (props) => {
  const styleClasses = 'w-full border-2 border-gray-light rounded-md h-12 bg-gray-lighter text-gray-dark font-body outline-none focus:shadow-sm'
  const options = props.options.map((option) => {
    return (
      <option
        key={option.value}
        value={option.value}
      >
        {option.label}
      </option>
    )
  })
  return (
    <select className={styleClasses}>
      <option value="" />
      {options}
    </select>
  )
}

Select.propTypes = {
  options: PropTypes.array,
  value: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ])
}


export default Select
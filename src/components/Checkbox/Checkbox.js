import React, { useState } from 'react'
import PropTypes from 'prop-types'

const Checkbox = (props) => {
  const [selected, setSelected] = useState(props.value)

  const handleChange = () => {
    setSelected(!selected)
  }

  return (
    <div className="checkbox">
      <label className={`${selected ? 'bg-primary border-primary' : 'border-gray-light' } border-2 rounded w-5 h-5 inline-block cursor-pointer`}>
        <img src="/checkmark.svg" />
        <input
          checked={selected}
          className="hidden"
          onChange={handleChange}
          type="checkbox"
        />
      </label>
    </div>
  )
}

Checkbox.propTypes = {
  value: PropTypes.bool
}

export default Checkbox
import React, { useEffect } from 'react';
import Referral from 'views/Referral/Referral'
import clientConfig from './config'
import WebFont from 'webfontloader';

if (clientConfig.fonts) {
  WebFont.load({
    google: {
      families: clientConfig.fonts
    }
  });
}

const App = () => {
  // console.log('config', clientConfig, process.env);
  useEffect(() => {
    console.log(process.env);
    console.log(clientConfig);
  })
  return (
    <div className="App font-body min-h-screen">
      <Referral />
    </div>
  );
}

export default App;

const merge = require('merge')
const base = require('./base/config')

console.log('config file', process.env);

const brand = require(`./${process.env.REACT_APP_BRAND ? process.env.REACT_APP_BRAND : 'default'}/config`)

module.exports = merge(base, brand)

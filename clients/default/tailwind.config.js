module.exports = {
  purge: [],
  theme: {
    extend: {
      fontFamily: {
        display: ['Larsseit-Bold', 'Arial', 'sans-serif'],
        body: ['Larsseit', 'Arial', 'sans-serif']
      },
      colors: {
        // 'primary': '#369de0',
        // 'primary-light': '#5bbce9',
        // 'primary-lighter': '#d5effb',
        'gray': '#f6fbfe',
        'gray-border': '#e1e8ef',
        'gray-light': '#edf1f6',
        'gray-lighter': '#fafcfd',
        'gray-dark': '#43464f'
      }
    }
  }
}

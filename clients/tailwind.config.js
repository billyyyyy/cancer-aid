// const merge = require('merge')
const merge = require('lodash.merge')
const base = require('./base/tailwind.config')
const brand = require(`./${process.env.REACT_APP_BRAND ? process.env.REACT_APP_BRAND : 'default'}/tailwind.config`)

module.exports = merge(base, brand)

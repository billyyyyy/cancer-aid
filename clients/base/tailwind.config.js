module.exports = {
  purge: [],
  theme: {
    extend: {
      minWidth: (theme) => ({
        ...theme('spacing')
      }),
      width: {
        1280: '1280px'
      },
      boxShadow: {
        button: '0 10px 15px -3px var(--main-color-lighter)'
      },
      colors: {
        'primary': 'var(--main-color)',
        'primary-light': 'var(--main-color-light)',
        'primary-lighter': 'var(--main-color-lighter)',
        'gray': '#f6fbfe',
        'gray-border': '#e1e8ef',
        'gray-light': '#edf1f6',
        'gray-lighter': '#fafcfd',
        'gray-dark': '#43464f'
      }
    },
  },
  variants: {},
  plugins: [],
}

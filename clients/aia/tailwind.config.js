module.exports = {
  purge: [],
  theme: {
    extend: {
      fontFamily: {
        display: ['Lobster', 'monospace'],
        body: ['Ubuntu Mono', 'Arial', 'sans-serif']
      },
      spacing: (theme) => ({
        ...theme.spacing,
        12: '2rem'
      }),
      // colors: {
      //   'primary': '#c53030',
      //   'primary-light': '#e53e3e',
      //   'primary-lighter': '#ffd5d5',
      //   'gray': '#fff0f0',
      //   'gray-border': '#e1e8ef',
      //   'gray-light': '#edf1f6',
      //   'gray-lighter': '#fafcfd',
      //   'gray-dark': '#43464f'
      // }
      // colors: {
      //   'primary': 'var(--main-color)',
      //   'primary-light': 'var(--main-color-light)',
      //   'primary-lighter': 'var(--main-color-lighter)',
      //   'gray': '#f6fbfe',
      //   'gray-border': '#e1e8ef',
      //   'gray-light': '#edf1f6',
      //   'gray-lighter': '#fafcfd',
      //   'gray-dark': '#43464f'
      // }
    }
  }
}

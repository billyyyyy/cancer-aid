/*
  create-react-app forces the root as /src
  if we want to keep the clients at the project root
  we have to add this file to override the default setting and set the alias path
  https://stackoverflow.com/questions/58243049/several-alias-without-ejecting-in-react
*/

const { override, addWebpackAlias, removeModuleScopePlugin } = require('customize-cra');
const path = require('path'); 

// console.log('in config overrides', process.env);

module.exports = override(
  removeModuleScopePlugin(),
  addWebpackAlias({
    clients: path.resolve('./clients')
  })
)
## To start the project

`yarn start`

&nbsp;
&nbsp;
&nbsp;

As I know CancerAid is using React, even my most recent experience is building Vue.js projects, I tried to build it in React to show I can pick it up.

## Tech stack used
- React
- Tailwind
- Babel
- Eslint
- Prettier

Also, I built the UI with responsive layout, which is totally mobile friendly.

## TODO

There are a few items I planned to do but haven't got enough time to finish.

- Input component to be able to support phone mask and date picker
- Using Redux to handle form data
- Checking the form by using `validate.js`
- Sending the form data to a mock endpoint
- Unit testing Input, Select, Checkbox and Button these general components
- Enhancing the accessibility

